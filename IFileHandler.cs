﻿using System;
using System.IO;
using Bitz.Modules.Core.Foundation;

namespace Bitz.Modules.Core.Assets
{
    public interface IFileHandler : IInjectable
    {
        Byte[] GetFile(String filename);
        Stream GetFileStream(String filename);
    }
}
