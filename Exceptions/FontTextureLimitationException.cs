﻿using System;
#if !WEB
using System.Runtime.Serialization;
#endif

namespace Bitz.Modules.Core.Foundation.Exceptions
{
    /// <inheritdoc />
    /// <summary>
    /// Triggered when the source textures across a font differ, this is a current limitation of the engine that all characters from a font must be sourced from the same source texture.
    /// </summary>
    [Serializable]
    public class FontTextureLimitationException : Exception
    {
        public FontTextureLimitationException()
        {
        }

        public FontTextureLimitationException(String message) : base(message)
        {
        }

        public FontTextureLimitationException(String message, Exception innerException) : base(message, innerException)
        {
        }
#if !WEB

        protected FontTextureLimitationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
#endif
    }
}