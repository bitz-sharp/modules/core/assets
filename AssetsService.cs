﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Bitz.Modules.Core.Assets;
using Bitz.Modules.Core.Foundation.Pack;
using Bitz.Modules.Core.Foundation.Debug.Logging;
using Bitz.Modules.Core.Foundation.Services;
#if !WEB
using ObjLoader.Loader.Loaders;
#endif
using Sbatman.Serialize;

#if ANDROID
using Bitz.Modules.Android.Platform;
using System.Text.RegularExpressions;
#endif
#if DESKTOP
#endif

namespace Bitz.Modules.Core.Foundation
{
    /// <summary>
    ///     The assets service is used for loading and sourcing all games assets at runtime
    /// </summary>
    public class AssetsService : Service, IAssetsService
    {
        private readonly List<String> _LoadedGraphicsPacks = new List<String>();
        private readonly List<SourceTexture> _LoadedSourceTextures = new List<SourceTexture>();
        private readonly List<Texture> _LoadedTextures = new List<Texture>();
#if !WEB
        private readonly List<ModelData> _LoadedModelData = new List<ModelData>();
#endif

        private readonly ILoggerService _LoggerSerivce;
        private readonly IFileHandler _FileHandler;
        public Texture WhitePixel { get; private set; }

        public Texture BlackPixel { get; private set; }

        public AssetsService()
        {
            _LoggerSerivce = Injector.GetSingleton<ILoggerService>();
            _FileHandler = Injector.GetSingleton<IFileHandler>();
        }

        protected override void Update(TimeSpan timeSinceLastUpdate)
        {
        }

        public override void Initialize()
        {
            CreateWhitePixelTexture();
            CreateBlackPixelTexture();
        }

        private void CreateWhitePixelTexture()
        {
            SourceTexture source = new SourceTexture(new Byte[] { 255, 255, 255, 255 }, 1, 1);
            _LoadedSourceTextures.Add(source);
            WhitePixel = new Texture(source, "System\\WhitePixel", 0, 0, 1, 1);
            _LoadedTextures.Add(WhitePixel);
        }

        private void CreateBlackPixelTexture()
        {
            SourceTexture source = new SourceTexture(new Byte[] { 0, 0, 0, 255 }, 1, 1);
            _LoadedSourceTextures.Add(source);
            BlackPixel = new Texture(source, "System\\BlackPixel", 0, 0, 1, 1);
            _LoadedTextures.Add(BlackPixel);
        }

        protected override void Shutdown()
        {
        }

        public override void Dispose()
        {
            _LoadedSourceTextures.Clear();
            _LoadedTextures.Clear();
            _LoadedGraphicsPacks.Clear();
            base.Dispose();
        }

        public override String GetServiceDebugStatus()
        {
            return $"AS:LoadedT={_LoadedTextures.Count},LoadedS={_LoadedSourceTextures.Count}";
        }

        public Byte[] GetFile(String filename)
        {
            return _FileHandler.GetFile(filename);
        }


        public Stream GetFileStream(String filename)
        {
            return _FileHandler.GetFileStream(filename);
        }

        /// <summary>
        ///     This function sources a GraphicsPack Packet from the file specified at the path provided and then loads it
        /// </summary>
        /// <param name="filename">The url from the current working directory that points to the pack file to load</param>
        /// <returns>True if loading succeeds else false</returns>
        public Boolean BuildPack(String filename)
        {
            return LoadGraphicsBytePack(GetFile(filename));
        }


        /// <summary>
        ///     Loads the provided byte array in as a graphicsPack Packet. This will load all textures and source textures present
        ///     in the contained texturepacks
        /// </summary>
        /// <param name="graphicsPackByteArray">A byte array representing a valid GraphicsPack packet</param>
        /// <returns>True if loading succeeds else false</returns>
        private Boolean LoadGraphicsBytePack(Byte[] graphicsPackByteArray)
        {
            try
            {
                Packet packet = Packet.FromByteArray(graphicsPackByteArray);
                GraphicsPack graphicsPack = new GraphicsPack(packet);
                _LoggerSerivce.Log(LogSeverity.VERBOSE, $"Loading Graphics Pack '{graphicsPack.Name}'");
                if (_LoadedGraphicsPacks.Contains(graphicsPack.Name))
                {
                    _LoggerSerivce.Log(LogSeverity.WARNING, $"Loading Graphics Pack failed as a pack known as '{graphicsPack.Name}' is already loaded in to the assets service");
                    return false;
                }

                foreach (TexturePack pack in graphicsPack.TexturePacks) LoadTexturePack(pack);
                _LoadedGraphicsPacks.Add(graphicsPack.Name);
            }
            catch (Exception e)
            {
                _LoggerSerivce.Log(LogSeverity.ERROR, $"Loading Graphics Pack failed with exception: '{e}'");
                return false;
            }
            return true;
        }

        /// <summary>
        ///     Loads the provided texture pack into the system, allocating the source texture and generating a number of nextures
        ///     for each PackedElement present in the texture pack
        /// </summary>
        /// <param name="texturePack">The texture pack to load</param>
        internal void LoadTexturePack(TexturePack texturePack)
        {
            SourceTexture sourceTexture = new SourceTexture(texturePack.GetSourceTextureMemory(), texturePack.Width, texturePack.Height) { CompressionFormat = texturePack.Compression };
            _LoadedSourceTextures.Add(sourceTexture);
            _LoggerSerivce.Log(LogSeverity.VERBOSE, $"Processing {texturePack.Compression} Texture Pack with {texturePack.GetPackedElements().Count()} sub textures");
            foreach (TexturePack.PackedElement element in texturePack.GetPackedElements())
            {
                _LoadedTextures.Add(new Texture(sourceTexture, element.OrigionalURL, element.X / (Single)texturePack.Width, element.Y / (Single)texturePack.Height, element.Width / (Single)texturePack.Width, element.Height / (Single)texturePack.Height));
            }
        }
#if !WEB
        private class MaterialLoader : IMaterialStreamProvider
        {
            private readonly String _Directory;
            public MaterialLoader(String directoryName)
            {
                _Directory = directoryName;
            }

            public Stream Open(String materialFilePath)
            {
                return File.OpenRead(_Directory + "\\" + materialFilePath);
            }
        }

        public void LoadModelData(String filename)
        {
            ObjLoaderFactory objLoaderFactory = new ObjLoaderFactory();

            IObjLoader objLoader = objLoaderFactory.Create(new MaterialLoader(Path.GetDirectoryName(filename)));

            _LoadedModelData.Add(new ModelData(filename, objLoader.Load(GetFileStream(filename))));
        }
#endif

        public Texture GenerateSubTexture(Texture orig, String name, Int32 xPos, Int32 yPos, Int32 width, Int32 height)
        {
            Single newXPos = orig.Uvx + xPos / (Single)orig.SourceTexture.Width;
            Single newYPos = orig.Uvy + yPos / (Single)orig.SourceTexture.Height;
            Single newUVWidth = width / (Single)orig.SourceTexture.Width;
            Single newUVHeight = height / (Single)orig.SourceTexture.Height;

            Texture newTexture = new Texture(orig.SourceTexture, name, newXPos, newYPos, newUVWidth, newUVHeight);
            _LoadedTextures.Add(newTexture);
            return newTexture;
        }

        /// <summary>
        ///     Returns the texture identified by the provided name
        /// </summary>
        /// <param name="name">The name of the texture (including path from the assets root during packing) to return</param>
        /// <returns>Returns the texture if found else returns null</returns>
        public Texture GetTexture(String name)
        {
            Texture result = _LoadedTextures.FirstOrDefault(a => a.Name.Equals(name));
            if (result == null) _LoggerSerivce.Log(LogSeverity.WARNING, $"A request to GetTexture for {name} returned null");
            return result;
        }
#if !WEB
        public ModelData GetModelData(String name)
        {
            return _LoadedModelData.FirstOrDefault(a => a.Name.Equals(name));
        }
#endif

    }

}