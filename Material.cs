﻿using System;
using OpenTK;

namespace Bitz.Modules.Core.Foundation
{
    public class Material
    {
        public String Name { get; }

        public Vector3 SpecularColour { get; set; }

        public Single SpecularHardness { get; set; }

        public Single Alpha { get; set; }

        public Texture DiffuseTexture { get; set; }

        public Material(String name)
        {
            Name = name;
        }

        public static String GetGLSLDefinition(Int32 count)
        {
            return $@"
uniform vec3 MAT_SpecularColour[{count}];
uniform float MAT_SpecularHardness[{count}];
uniform float MAT_Alpha[{count}];
uniform sampler2D MAT_DiffuseTexture[{count}];
";
        }
    }
}
