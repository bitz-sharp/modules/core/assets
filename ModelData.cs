﻿#if !WEB
using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation.Logic;
using ObjLoader.Loader.Data.Elements;
using ObjLoader.Loader.Data.VertexData;
using ObjLoader.Loader.Loaders;
using OpenTK;

namespace Bitz.Modules.Core.Foundation
{
    public class ModelData : BasicObject
    {
        private readonly Vector3[] _Verts;
        private readonly UInt32[] _Indicies;
        private readonly Vector2[] _TextureCoordinates;
        private readonly Vector4[] _DiffuseColours;
        private readonly Vector3[] _Normals;
        private readonly Int32[] _MaterialIDs;
        private readonly Material[] _Materials;

        public String Name { get; }

        public Vector3[] Verts => _Verts.ToArray();
        public UInt32[] Indicies => _Indicies.ToArray();
        public Vector2[] TextureCoordinates => _TextureCoordinates.ToArray();
        public Vector4[] DiffuseColours => _DiffuseColours.ToArray();
        public Vector3[] Normals => _Normals.ToArray();
        public Int32[] MaterialIDs => _MaterialIDs.ToArray();
        public Material[] Materials => _Materials.ToArray();

        internal ModelData(String name, Vector3[] verts)
        {
            Name = name;
            _Verts = verts;
        }

        public ModelData(String name, LoadResult result)
        {
            Name = name;
            List<Vector3> verts = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<UInt32> indicies = new List<UInt32>();
            List<Vector2> textureCoordinates = new List<Vector2>();
            List<Vector4> diffuseColours = new List<Vector4>();
            List<Int32> materialIDs = new List<Int32>();
            List<Tuple<Byte, Material>> materials = new List<Tuple<Byte, Material>>();

            Int32 vertsAdded = 0;
            foreach (Group group in result.Groups)
            {
                Vector4 diffuseColour = Vector4.One;

                Int32? materialID = GetMaterialID(result, group, materials);

                if (group.Material != null)
                {
                    diffuseColour = new Vector4(group.Material.DiffuseColor.X, group.Material.DiffuseColor.Y, group.Material.DiffuseColor.Z, group.Material.Transparency);
                }
                else if (result.Materials[0] != null)
                {
                    diffuseColour = new Vector4(result.Materials[0].DiffuseColor.X, result.Materials[0].DiffuseColor.Y, result.Materials[0].DiffuseColor.Z, result.Materials[0].Transparency);
                }

                foreach (Face face in group.Faces)
                {
                    for (Int32 i = 0; i < face.Count; i++)
                    {
                        Vertex v = result.Vertices[face[i].VertexIndex - 1];
                        Normal n = face[i].NormalIndex != 0 ? result.Normals[face[i].NormalIndex - 1] : CalculateNromal(result.Vertices[face[0].VertexIndex - 1], result.Vertices[face[1].VertexIndex - 1], result.Vertices[face[2].VertexIndex - 1]);
                        ObjLoader.Loader.Data.VertexData.Texture t = result.Textures[face[i].TextureIndex - 1];
                        materialIDs.Add(materialID.Value);
                        verts.Add(new Vector3(v.X, v.Y, v.Z));
                        normals.Add(new Vector3(n.X, n.Y, n.Z));
                        diffuseColours.Add(diffuseColour);
                        textureCoordinates.Add(group.Material?.DiffuseTextureMap != null ? new Vector2(t.X, t.Y) : Vector2.Zero);
                        indicies.Add((UInt32)vertsAdded);

                        vertsAdded++;
                    }
                }
            }
            _Verts = verts.ToArray();
            _Normals = normals.ToArray();
            _TextureCoordinates = textureCoordinates.ToArray();
            _Indicies = indicies.ToArray();
            _DiffuseColours = diffuseColours.ToArray();
            _Materials = materials.Select(a => a.Item2).ToArray();
            _MaterialIDs = materialIDs.ToArray();
        }

        private Normal CalculateNromal(Vertex a, Vertex b, Vertex c)
        {
            Vector3 vA = new Vector3(a.X, a.Y, a.Z);
            Vector3 vB = new Vector3(b.X, b.Y, b.Z);
            Vector3 vC = new Vector3(c.X, c.Y, c.Z);
            Vector3 Dir = (vB - vA)*(vC - vA);
            Vector3 Norm = Dir/Dir.Length;
            return new Normal(Norm.X,Norm.Y,Norm.Z);
        }

        private static Int32? GetMaterialID(LoadResult result, Group @group, List<Tuple<Byte, Material>> materials)
        {
            ObjLoader.Loader.Data.Material mat = @group.Material ?? result.Materials[0];

            Int32? materialID = materials.FirstOrDefault(a => a.Item2.Name.Equals(mat.Name))?.Item1;
            if (materialID == null)
            {
                Material material = new Material(mat.Name)
                {
                    Alpha = mat.Transparency,
                    //DiffuseTexture = mat.DiffuseTextureMap
                    SpecularColour = new Vector3(mat.SpecularColor.X, mat.SpecularColor.Y, mat.SpecularColor.Z),
                    SpecularHardness = mat.SpecularCoefficient
                };
                materialID = materials.Count;

                materials.Add(new Tuple<Byte, Material>((Byte)materialID.Value, material));
            }
            return materialID;
        }
    }
}
#endif