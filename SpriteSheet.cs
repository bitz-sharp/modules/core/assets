﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bitz.Modules.Core.Foundation.Logic;

namespace Bitz.Modules.Core.Foundation
{
    public class SpriteSheet : BasicObject
    {
        private static readonly Random _RND = new Random();

        private readonly Texture[] _Frames;

        public Int32 NumberOfFrames => _Frames.Length;

        public Texture SourceTexture { get; }

        public SpriteSheet(SpriteSheet original)
        {
            _Frames = original._Frames.ToArray();
        }
        public SpriteSheet(Texture sourceTexture, Int32 frames, Int32 frameWidth, Int32 frameHeight, Single fps, Int32 startingFrame = 0)
        {
            SourceTexture = sourceTexture;
            Int32 xpos = 0;
            Int32 ypos = 0;

            List<Texture> textureFrames = new List<Texture>();
            for (Int32 i = 0; i < frames; i++)
            {
                textureFrames.Add(Injector.GetSingleton<IAssetsService>().GenerateSubTexture(sourceTexture, $"{sourceTexture.Name}_Frame{i}", xpos, ypos, frameWidth, frameHeight));
                xpos += frameWidth;
                if (xpos >= sourceTexture.Width)
                {
                    xpos = 0;
                    ypos += frameHeight;
                }
            }
            _Frames = textureFrames.ToArray();
        }

        public SpriteSheet(Texture[] textures, Single fps, Int32 startingFrame = 0)
        {
            _Frames = textures;
        }

        public virtual Texture GetRandomFrame()
        {
            return _Frames[_RND.Next(_Frames.Length)];
        }

        public virtual Texture[] GetAllFrames()
        {
            return _Frames;
        }

        public virtual Texture GetFrame(Int32 id)
        {
            if (id < 0 || id >= _Frames.Length) throw new ArgumentOutOfRangeException(nameof(id));
            return _Frames[id];
        }
    }
}