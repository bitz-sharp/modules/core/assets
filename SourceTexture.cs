﻿using System;
using Bitz.Modules.Core.Foundation.Pack;

namespace Bitz.Modules.Core.Foundation
{
    public class SourceTexture
    {
        public TexturePack.CompressionFormat CompressionFormat = TexturePack.CompressionFormat.RAW;
        public readonly Int32 Height;
        public readonly Int32 Width;

        public SourceTexture(Object sourceTextureData, Int32 width, Int32 height)
        {
            SourceTextureData = sourceTextureData;
            Width = width;
            Height = height;
        }

        public Object SourceTextureData { get; }
    }
}