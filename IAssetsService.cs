﻿using System;
using System.IO;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Foundation
{
    public interface IAssetsService : IService
    {
        Texture BlackPixel { get; }
        Texture WhitePixel { get; }

        Texture GenerateSubTexture(Texture orig, String name, Int32 xPos, Int32 yPos, Int32 width, Int32 height);
        Byte[] GetFile(String filename);
        Stream GetFileStream(String filename);
#if !WEB
        ModelData GetModelData(String name);
        void LoadModelData(String filename);
#endif
        Texture GetTexture(String name);
        Boolean BuildPack(String filename);

    }
}