﻿using System;
using Bitz.Modules.Core.Foundation.Interfaces;
using Bitz.Modules.Core.Foundation.Logic;
using OpenTK;

namespace Bitz.Modules.Core.Foundation
{
    public class Texture : BasicObject, ISizeable2D
    {
        public Texture(SourceTexture sourceTexture, String name, Single uvx, Single uvy, Single uvWidth, Single uvHeight)
        {
            Uvx = uvx;
            Uvy = uvy;
            UvWidth = uvWidth;
            UvHeight = uvHeight;
            Name = name;
            SourceTexture = sourceTexture;
        }
        public String Name { get;}
        public Single Uvx { get; }
        public Single Uvy { get; }
        public Single UvWidth { get; }
        public Single UvHeight { get; }

        public Vector2 Uvtl => new Vector2(Uvx, Uvy);
        public Vector2 Uvtr => new Vector2(Uvx + UvWidth, Uvy);

        public Vector2 Uvbl => new Vector2(Uvx, Uvy + UvHeight);
        public Vector2 Uvbr => new Vector2(Uvx + UvWidth, Uvy + UvHeight);

        public SourceTexture SourceTexture { get; private set; }

        public Single Width
        {
            get => SourceTexture.Width * UvWidth;
            set => throw new NotSupportedException();
        }

        public Single Height
        {
            get => SourceTexture.Height * UvHeight;
            set => throw new NotSupportedException();
        }

        public Vector2 Size
        {
            get => new Vector2(Width, Height);
            set => throw new NotSupportedException();
        }

        public override void Dispose()
        {
            SourceTexture = null;
            base.Dispose();
        }
    }
}