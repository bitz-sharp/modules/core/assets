﻿using System;
using System.Collections.Generic;
using Sbatman.Serialize;

namespace Bitz.Modules.Core.Foundation.Pack
{
    public class GraphicsPack : IDisposable
    {
        public GraphicsPack()
        {
        }

        public GraphicsPack(Packet packet)
        {
            Object[] objects = packet.GetObjects();
            Int32 objPart = 0;
            Name = (String) objects[objPart++];
            Int32 numberOfTexturePacks = (Int32) objects[objPart++];

            for (Int32 i = 0; i < numberOfTexturePacks; i++) TexturePacks.Add(new TexturePack((Packet) objects[objPart++]));

            packet.Dispose();
        }

        public String Name { get; set; }
        public List<TexturePack> TexturePacks { get; set; } = new List<TexturePack>();

        public void Dispose()
        {
            foreach (TexturePack pack in TexturePacks) pack.Dispose();
            TexturePacks.Clear();
        }

        public Packet ToPacket()
        {
            Packet returnPacket = new Packet(345);
            returnPacket.Add(Name);
            returnPacket.Add(TexturePacks.Count);
            foreach (TexturePack pack in TexturePacks) returnPacket.Add(pack.ToPacket());
            return returnPacket;
        }
    }
}