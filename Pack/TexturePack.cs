﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Sbatman.Serialize;

[assembly: InternalsVisibleTo("Bitz.Tools.Packer")]

namespace Bitz.Modules.Core.Foundation.Pack
{
    public class TexturePack : IDisposable
    {
        public enum CompressionFormat
        {
            RAW,
            DXT,
            PNG
        }

        private readonly List<PackedElement> _PackedElements = new List<PackedElement>();
        private Byte[] _SourceTextureMemory;

        public TexturePack()
        {
        }

        public TexturePack(Packet packet)
        {
            Object[] objects = packet.GetObjects();
            Int32 objPart = 0;
            _SourceTextureMemory = (Byte[])objects[objPart++];
            Width = (Int32)objects[objPart++];
            Height = (Int32)objects[objPart++];
            Compression = (CompressionFormat)(Int32)objects[objPart++];
            Int32 packedElements = (Int32)objects[objPart++];
            for (Int32 i = 0; i < packedElements; i++)
            {
                _PackedElements.Add(new PackedElement((Packet)objects[objPart++]));
            }
            packet.Dispose();
        }

        public Int32 Width { get; set; }
        public Int32 Height { get; set; }
        public CompressionFormat Compression { get; set; }

        public void Dispose()
        {
            _SourceTextureMemory = null;
            _PackedElements.Clear();
        }

        internal void AddElement(PackedElement element)
        {
            _PackedElements.Add(element);
        }

        internal void RemoveElement(PackedElement element)
        {
            _PackedElements.Remove(element);
        }

        public Byte[] Save()
        {
            Packet p = new Packet(1251);
            p.Add(_PackedElements.Count);
            foreach (PackedElement element in _PackedElements)
            {
                p.Add(element.OrigionalURL);
                p.Add(element.X);
                p.Add(element.Y);
                p.Add(element.Width);
                p.Add(element.Height);
            }

            p.Add(_SourceTextureMemory);

            return p.ToByteArray();
        }

        public static TexturePack Load(Byte[] byteArray)
        {
            TexturePack returnTexturePack = new TexturePack();

            Packet p = Packet.FromByteArray(byteArray);
            Object[] pObjects = p.GetObjects();

            Int32 dataPos = 0;
            Int32 numberOfPackedElements = (Int32)pObjects[dataPos++];
            for (Int32 i = 0; i < numberOfPackedElements; i++)
            {
                returnTexturePack._PackedElements.Add(new PackedElement
                {
                    OrigionalURL = (String)pObjects[dataPos++],
                    X = (Int32)pObjects[dataPos++],
                    Y = (Int32)pObjects[dataPos++],
                    Width = (Int32)pObjects[dataPos++],
                    Height = (Int32)pObjects[dataPos++]
                });
            }

            returnTexturePack._SourceTextureMemory = (Byte[])pObjects[dataPos];

            p.Dispose();

            return returnTexturePack;
        }

        public Byte[] GetSourceTextureMemory()
        {
            return _SourceTextureMemory;
        }

        internal IEnumerable<PackedElement> GetPackedElements()
        {
            return _PackedElements.ToList();
        }

        public void SetSourceTextureMemory(Byte[] data)
        {
            _SourceTextureMemory = data;
        }

        public Packet ToPacket()
        {
            Packet returnPacket = new Packet(456);
            returnPacket.Add(_SourceTextureMemory);
            returnPacket.Add(Width);
            returnPacket.Add(Height);
            returnPacket.Add((Int32)Compression);
            returnPacket.Add(_PackedElements.Count);
            foreach (PackedElement element in _PackedElements) returnPacket.Add(element.ToPacket());
            return returnPacket;
        }

        internal class PackedElement
        {
            internal String OrigionalURL;
            internal Int32 Width;
            internal Int32 Height;
            internal Int32 X;
            internal Int32 Y;

            public Packet ToPacket()
            {
                Packet returnPacket = new Packet(567);
                returnPacket.Add(OrigionalURL);
                returnPacket.Add(Width);
                returnPacket.Add(Height);
                returnPacket.Add(X);
                returnPacket.Add(Y);
                return returnPacket;
            }

            public PackedElement()
            {

            }

            public PackedElement(Packet packet)
            {
                Object[] objects = packet.GetObjects();
                OrigionalURL = (String)objects[0];
                Width = (Int32)objects[1];
                Height = (Int32)objects[2];
                X = (Int32)objects[3];
                Y = (Int32)objects[4];
                packet.Dispose();
            }
        }
    }
}